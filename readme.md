# Quantum Mechanical Keyboard Firmware

[![Current Version](https://img.shields.io/github/tag/qmk/qmk_firmware.svg)](https://github.com/qmk/qmk_firmware/tags)
[![Build Status](https://travis-ci.org/qmk/qmk_firmware.svg?branch=master)](https://travis-ci.org/qmk/qmk_firmware)
[![Discord](https://img.shields.io/discord/440868230475677696.svg)](https://discord.gg/Uq7gcHh)
[![Docs Status](https://img.shields.io/badge/docs-ready-orange.svg)](https://docs.qmk.fm)
[![GitHub contributors](https://img.shields.io/github/contributors/qmk/qmk_firmware.svg)](https://github.com/qmk/qmk_firmware/pulse/monthly)
[![GitHub forks](https://img.shields.io/github/forks/qmk/qmk_firmware.svg?style=social&label=Fork)](https://github.com/qmk/qmk_firmware/)

This is a keyboard firmware based on the [tmk\_keyboard firmware](https://github.com/tmk/tmk_keyboard) with some useful features for Atmel AVR and ARM controllers, and more specifically, the [OLKB product line](https://olkb.com), the [ErgoDox EZ](https://ergodox-ez.com) keyboard, and the [Clueboard product line](https://clueboard.co).

## Documentation

* [See the official documentation on docs.qmk.fm](https://docs.qmk.fm)

The docs are powered by [Docsify](https://docsify.js.org/) and hosted on [GitHub](/docs/). You can request changes by making a fork and [pull request](https://github.com/qmk/qmk_firmware/pulls), or by clicking the "Edit Document" link at the bottom of any page.

## Supported Keyboards

* [Planck](/keyboards/planck/)
* [Preonic](/keyboards/preonic/)
* [ErgoDox EZ](/keyboards/ergodox_ez/)
* [Clueboard](/keyboards/clueboard/)
* [Cluepad](/keyboards/clueboard/17/)
* [Atreus](/keyboards/atreus/)

The project also includes community support for [lots of other keyboards](/keyboards/).

## Maintainers

QMK is developed and maintained by Jack Humbert of OLKB with contributions from the community, and of course, [Hasu](https://github.com/tmk). The OLKB product firmwares are maintained by [Jack Humbert](https://github.com/jackhumbert), the Ergodox EZ by [ZSA Technology Labs](https://github.com/zsa), the Clueboard by [Zach White](https://github.com/skullydazed), and the Atreus by [Phil Hagelberg](https://github.com/technomancy).

## Official Website

[qmk.fm](https://qmk.fm) is the official website of QMK, where you can find links to this page, the documentation, and the keyboards supported by QMK.

## Steps to Flash Firmware After Key Map Update

All the keyboard layouts are found in the [./keyboards/](./keyboards/) directory.  The one that I'm using right now is a modification of the Mac specific [tmd/alicia/](./keyboards/tmd/alicia) keymap.  In order to make updates to this key map and flash it onto the board follow these steps:

1. Update the [keymap.c](./keyboards/tmd/alicia/keymaps/chris_mac/keymap.c) file in the [chris_mac/](./keyboards/tmd/alicia/keymaps/chris_mac/) in the directory withe new layout you'd like.

1. From the root of this directory (for some reason) run this `Make` command to spit out a `.hex` file that can be loaded into the QMK Toolkit.  This will drop the file in the root of the directory here.

        ➜  qmk_firmware: make tmd/alicia:chris_mac
        QMK Firmware 0.6.275
        Making tmd/alicia with keymap chris_mac

        avr-gcc (GCC) 8.3.0
        Copyright (C) 2018 Free Software Foundation, Inc.
        This is free software; see the source for copying conditions.  There is NO
        warranty; not even for MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.

        Size before:
          text    data     bss     dec     hex filename
              0   25238       0   25238    6296 .build/tmd_alicia_chris_mac.hex

        Copying tmd_alicia_chris_mac.hex to qmk_firmware folder [OK]
        Checking file size of tmd_alicia_chris_mac.hex

1. Put the keyboard into `RESET` mode in order to load a new firmware config file.
    
        Function + R or  (i.e. MO(_FN1) + RESET)

1. Open QMK Toolbox, load the file, and then 'Flash'.
