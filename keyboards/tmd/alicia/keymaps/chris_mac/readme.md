# The default keymap for alicia - intended for use on Macs
# 		
# Split right shift for an Additional Command key (not required) - split backspace or single backspace both mapped to backspace
```
# spit out a .hex file to load into QMK Toolbox
make tmd/alicia:chris_mac # 'chris_mac' is the directory name
```
```
# Put the keyboard into 'shut off in order to load a new firmware config' mode
left star + right ctrl (i.e. MO(_FN1) + RESET)

Open QMK Toolbox, load the file and then 'Flash'