# TMD Alicia

![Alicia](https://i.imgur.com/ctOAq1l.jpg)

A TGR-Alice-compatible homage PCB (with SMT per-key RGB and/or RGB strips) - designed for use with themillerdave's TMD Alicia stacked aluminum cases

* Keyboard Maintainer: [themillerdave](https://github.com/themillerdave)  
* Hardware Supported: ATMEGA32U4 custom designed PCB   
* Hardware Availability: hit up /u/themillerdave on reddit or email dave@themmillerdave.com

Make example for this keyboard (after setting up your build environment):

    make tmd/alicia:default  # (for Windows usage)
    # OR
    make tmd/alicia:mac # (for those of us who prefer Apple computers)

See the [build environment setup](https://docs.qmk.fm/#/getting_started_build_tools) and the [make instructions](https://docs.qmk.fm/#/getting_started_make_guide) for more information. Brand new to QMK? Start with our [Complete Newbs Guide](https://docs.qmk.fm/#/newbs).
